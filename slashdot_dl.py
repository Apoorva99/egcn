import utils as u
import os

import tarfile

import torch
import pickle
import pandas as pd

def load_data_from_tar(file, tar_archive, replace_unknow=False, starting_line=1, sep=',', type_fn = float, tensor_const = torch.DoubleTensor):
    f = tar_archive.extractfile(file)
    lines = f.read()#
    lines=lines.decode('utf-8')
    if replace_unknow:
        lines=lines.replace('unknow', '-1')
        lines=lines.replace('-1n', '-1')

    lines=lines.splitlines()

    data = [[type_fn(r) for r in row.split()] for row in lines[starting_line:]]
    data = tensor_const(data)
    #print (file,'data size', data.size())
    return data

class slashdot_Dataset():
    def __init__(self,args):
        args.uc_irc_args = u.Namespace(args.mit_args)
        self.edges = self.load_edges()
        print(self.edges)        
        
    def load_edges(self):

        with open('data/slashdot_monthly_dynamic.pkl', 'rb') as f:
            data = pickle.load(f)
        c = 0
        source_arr = []
        time_arr = []
        target_arr = []
        weight_arr = []
        for network in data:
            for i in data[network].edges:
                time_arr.append(c*14000)
                source_arr.append(i[0])
                target_arr.append(i[1])
                weight_arr.append(1)

            c = c+1
        df = pd.DataFrame({'Source': source_arr, 'Target': target_arr,'weight':weight_arr,'time':time_arr})
        df.columns = ['source', 'target','weight','time']

        data = torch.DoubleTensor(df.values.tolist())
        data = data.long()

        cols = u.Namespace({'source': 0,
							 'target': 1,
							 'weight': 2,
							 'time': 3})
        
        self.num_nodes = int(data[:,[cols.source,cols.target]].max())

        #first id should be 0 (they are already contiguous)
        data[:,[cols.source,cols.target]] -= 1

        #add edges in the other direction (simmetric)
        data = torch.cat([data,
                           data[:,[cols.target,
                                   cols.source,
                                   cols.weight,
                                   cols.time]]],
                           dim=0)

        data[:,cols.time] = u.aggregate_by_time(data[:,cols.time],
                                    14000)

        ids = data[:,cols.source] * self.num_nodes + data[:,cols.target]
        self.num_non_existing = float(self.num_nodes**2 - ids.unique().size(0))

        idx = data[:,[cols.source,
                      cols.target,
                      cols.time]]

        self.max_time = data[:,cols.time].max()
        self.min_time = data[:,cols.time].min()


        return {'idx': idx, 'vals': torch.ones(idx.size(0))}

        
        
        
        

        